package PageTest;
import java.util.concurrent.TimeUnit;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Shopbridge.BaseClass;
import Shopbridge.LandingPage;
import Shopbridge.LoginValidation;
import Shopbridge.DropdownValidation;

public class TestCaseRunner extends BaseClass{
	
    LoginValidation LogIn;
    DropdownValidation drop;
    @BeforeTest
    public void NavigationToURL(){
    	BaseClass.invokeBrowser("Chrome");
    	LandingPage.shopBridge();
    	driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
    }
    
	@Test(priority=0)
    public void DropdownValidation() throws InterruptedException{
		DropdownValidation.verifyLanguage();
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
    	
    }
    @Test(priority=1)
    public void Login(){
    	LoginValidation.Login();
    	driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
    }
}
