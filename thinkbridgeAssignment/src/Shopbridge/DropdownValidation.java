package Shopbridge;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class DropdownValidation extends BaseClass {


	public static void verifyLanguage() throws InterruptedException{
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='language']/div[1]")).click();
		 List<WebElement> allOptions = driver.findElements(By.cssSelector(".ui-select-choices-row"));
		
	        System.out.println(allOptions.size());
	              
	  
	         for(int i = 0; i<allOptions.size(); i++) {
	             
	             
	            if(allOptions.get(i).getText().contains("English")) {
	                 
	                System.out.println("English is a choice in Dropdown");
	                 
	            }
	            if(allOptions.get(i).getText().contains("Dutch")) {
	                 
	                System.out.println("Dutch is a choice in Dropdown");
	                 
	            }
	        }
		
	}
}
