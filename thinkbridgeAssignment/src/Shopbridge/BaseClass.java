package Shopbridge;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;


public class BaseClass {
	public static  WebDriver driver;
	public static void invokeBrowser(String browserName) {

		try {

			if (browserName.equalsIgnoreCase("Chrome")) {
				System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"//driver//chromedriver(1)//chromedriver.exe");
				   driver=new ChromeDriver();
			}
			 else if (browserName.equalsIgnoreCase("Mozila")) {
				System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "//driver//geckodriver-v0.26.0-win64//geckodriver.exe");
				driver = new FirefoxDriver();
		    } 
			
		} 
		catch (Exception e) {
			System.out.println(e.getMessage());
		}
		

}
	@AfterTest
	public void closeBrowser(){
		driver.quit();
	}
}
