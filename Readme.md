## Please refer Shopbridge_Assignment_Question_01 file for Question 1 of Assignment


## Please refer thinkbridgeAssignment folder for Question 2(coding) of Assignment

1. Click **SRC** folder 📂 .
2. Click the **Shopbridge** 📂 folder present inside **SRC**.
3. **Shopbridge** contains 3 files **BaseClass** , **LandingPage**, **DropdownValidation**, **LoginValidation**
4. The 3 files are  created to cover different functionality require for testing.
**BaseClass** contains Browser invoker can be used for testing several scripts.
**LandingPage** conatins code to navigates the user to the site "https://jt-dev.azurewebsites.net/#/SignUp" .**DropdownValidation** cover Language validation functionality.
**LoginValidation** cover the Login functionality.
5. Click on **PageTest** 📂 folder , It contains **TestCaseRunner** file,  that is  used for the test case execution
 
**Language**-Java



## Please refer Question 3_Assignment file for Question 3 of Assignment

